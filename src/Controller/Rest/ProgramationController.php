<?php

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Programation;
use App\Service\ApiYoutube;

class ProgramationController extends FOSRestController
{
    /**
     * @Rest\Get("/programation")
     * @param Request $request
     * @param \App\Service\ApiYoutube $apiYoutube
     */
    public function index(Request $request, ApiYoutube $apiYoutube)
    {
        $entityManager = $this->getDoctrine()->getManager();
    
        $programacion = $entityManager->getRepository(Programation::class)->findNextVideos($apiYoutube);

        return new JsonResponse(
            $programacion,
            JsonResponse::HTTP_OK
       );
    }

    /**
     * @Rest\Delete("/programation/{id}")
     * @param Request $request
     * @param String $id
     * @return JsonResponse
     */
    public function remove(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $programation = $entityManager->getRepository(Programation::class)->find($id);

        if (empty($programation)) {
            return new JsonResponse(
                ['id' => $id],
                JsonResponse::HTTP_NOT_FOUND
            );
        }

        $entityManager->remove($programation);
        $entityManager->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }
}
