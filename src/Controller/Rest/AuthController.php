<?php

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Firebase\JWT\JWT;

use App\Entity\User;

class AuthController extends FOSRestController
{
    /**
     * @Rest\Post("/register")
     * @param Request $request
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('it.music.colombia@gmail.com')
        ->setTo('osmarvalero100@gmail.com')
        ->setBody('<p>Iniciar Sesión</p> https://itmusic.herokuapp.com/');

        
       if ($mailer->send($message)) {
           echo 'ok';
       } 
        exit;
        $em = $this->getDoctrine()->getManager();
        
        $username = $request->get('username');
        $password = $request->get('password');
        $email = $request->get('email');
        
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($encoder->encodePassword($user, $password));
        $user->setEmail($email);
        $em->persist($user);
        $em->flush();

        // enviar mail $_ENV['MAILER_URL']]; .env

        return new JsonResponse(
            ['id' => $user->getId()],
            JsonResponse::HTTP_OK
       );
    }

    /**
     * @Rest\Get("/login")
     * @param Request $request
     */
    public function login(Request $request) {
        $jwt = JWT::encode(['id'=>1], getEnv('KEY_SECRET'));
        // print_r(JWT::decode($jwt, getEnv('KEY_SECRET'), array('HS256')));
        return new JsonResponse(
            ['token' => 'Bearer '. $jwt],
            JsonResponse::HTTP_OK
       );
    }
  
    public function api()
    {
        return new Response(sprintf('Logged in as %s', $this->getUser()->getUsername()));
    }
    
    public function login_check() {
    }

    /**
     * @Rest\Get("/ip")
     * @param Request $request
     */
    public function ip(Request $request)
    {
        $data = [];
        if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            $data['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            $data['HTTP_X_FORWARDED_FOR'] = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        if (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            $data['HTTP_X_FORWARDED'] = $_SERVER["HTTP_X_FORWARDED"];
        }
        if (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            $data['HTTP_FORWARDED_FOR'] = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        if (isset($_SERVER["HTTP_FORWARDED"]))
        {
            $data['HTTP_FORWARDED'] = $_SERVER["HTTP_FORWARDED"];
        }
        if (isset($_SERVER["REMOTE_ADDR"]))
        {
            $data['REMOTE_ADDR'] = $_SERVER["REMOTE_ADDR"];
        }
        

        foreach ( [ 'HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR' ] as $key ) {
            // Comprobamos si existe la clave solicitada en el array de la variable $_SERVER 
            if ( array_key_exists( $key, $_SERVER ) ) {
    
                // Eliminamos los espacios blancos del inicio y final para cada clave que existe en la variable $_SERVER 
                foreach ( array_map( 'trim', explode( ',', $_SERVER[ $key ] ) ) as $ip ) {
    
                    // Filtramos* la variable y retorna el primero que pase el filtro
                    if ( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) !== false ) {
                        $data['otra'] = $ip;
                    }
                }
            }
        }
    
        
        return new JsonResponse(
            $data,
            JsonResponse::HTTP_OK
       );
    }
    
}