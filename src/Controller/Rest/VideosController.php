<?php

namespace App\Controller\Rest;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Service\ApiYoutube;
use App\Service\DataRequest;
use App\Entity\Programation;
use App\Entity\YoutubeVideos;

class VideosController extends FOSRestController
{
    /**
     * @Rest\Get("/videos")
     * @param Request $request
     */
    public function search(Request $request, ApiYoutube $apiYoutube)
    {
        $q = $request->get('q');
        $searchResponse = $apiYoutube->search($q);

        if (!empty($searchResponse)) {
            foreach ($searchResponse['items'] as $searchResult) {
                if ($searchResult['id']['kind'] == 'youtube#video') {
                    $data[]= [
                        "video_id" => $searchResult['id']['videoId'],
                        "title" => $searchResult['snippet']['title'],
                        "thumbnails" => $searchResult['snippet']['thumbnails'],
                        "duration" => $searchResult['contentDetails']['duration'],
                    ];
                }
            }
        } else {
            return new JsonResponse(array('message' => 'Failed conection Youtube'), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return new JsonResponse(
            $data,
            JsonResponse::HTTP_OK
       );
    }

    /**
     * @Rest\Get("/videos/next")
     * @param Request $request
     */
    public function next(Request $request)
    {
        $videos = $this->getDoctrine()->getRepository(YoutubeVideos::class)->findNextVideos();

        return new JsonResponse(
            $videos,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Rest\Post("/videos")
     * @param Request $request
     */
    public function create(Request $request, ApiYoutube $apiYoutube, DataRequest $dataReq)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $djIp = $dataReq->getRealIP();
        
        //$djInProgramation = $entityManager->getRepository(Programation::class)->findByDjIp($djIp);
        // Si el dispositivo que solicita el video no tiene videos en lista por reproducción se agrega el video a la lista
        //if (empty($djInProgramation)) {
            $youtubeId = $request->get('id');
            $infoVideo = $apiYoutube->getVideoInfo($youtubeId);

            if (empty($infoVideo)) {
                $infoVideo = $entityManager->getRepository(YoutubeVideos::class)->getDefaultInfoVideo();
            }

            //var_dump($infoVideo['items'][0]['contentDetails']['duration']);exit;
            // Se valida que el video exista en Youtube
            if (!empty($infoVideo)) {
                // Solo se permiten videos de máximo 15 minutos
                if ($infoVideo['items'][0]['contentDetails']['duration'] <= 900) {
                    $programation = new Programation();
                    $video = $this->getDoctrine()->getRepository(YoutubeVideos::class)->findByYoutubeId($youtubeId);
                    // Si la canción  NO está registrada en db hay que registrarla
                    if (empty($video)) {
                        // Se hace el registro de la nueva canción
                        $video = new YoutubeVideos();
                        $video->setYoutubeId($youtubeId);
                        $video->setDuration($infoVideo['items'][0]['contentDetails']['duration']);
                        $video->setTitle($infoVideo['items'][0]['snippet']['title']);
                        $video->setThumbnails($infoVideo['items'][0]['snippet']['thumbnails']);
                        
                        $entityManager->persist($video);
                        $entityManager->flush();

                        $programation->setYoutubeVideo($video);
                    } else {
                        $video = $this->getDoctrine()->getRepository(YoutubeVideos::class)->find($video[0]->getId());
                        $programation->setYoutubeVideo($video);
                    }

                    // Se agrega la canción a la cola de reproducción
                    $programation->setDjIp($djIp);
                    $entityManager->persist($programation);
                    $entityManager->flush();
                    $data = ['success' => '¡Hecho!. Pronto veras tu vídeo y prodras programar otro.'];
                } else {
                    $data = ['error' => 'Por favor, programar vídeos de máximo 15 minutos de duración.'];
                }
            } else {
                $data = ['error' => 'El vídeo selecionado no está disponible.'];
            }
        /*} else {
            $data = ['error' => '¡No comas ansias!. A penas reproduzcamos tu vídeo podrás programar otro.'];
        }*/

        return new JsonResponse(
            $data,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Rest\Delete("/videos/{id}")
     * @param Request $request
     * @param String $id
     * @return JsonResponse
     */
    public function remove(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $video = $entityManager->getRepository(YoutubeVideos::class)->find($id);

        if (empty($video)) {
            return new JsonResponse(
                ['id' => $id],
                JsonResponse::HTTP_NOT_FOUND
            );
        }

        $entityManager->remove($video);
        $entityManager->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    /**
     * Actualiza los datos de video ingresados por URL y sin créditos de Youtube
     * @Rest\Put("/videos/info")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateInfo(Request $request, ApiYoutube $apiYoutube)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $repoVideos = $entityManager->getRepository(YoutubeVideos::class);
        $videos = $repoVideos->findByDuration(234);

        $countUpdate = 0;

        if (!empty($videos)) {
            foreach ($videos as $key => $value) {
                $youtubeId = $value->getYoutubeId();
                $video = $repoVideos->find($value->getId());
                if (!empty($video)) {
                    $infoVideo = $apiYoutube->getVideoInfo($youtubeId);
                    if (!empty($infoVideo)) {
                        $video->setDuration($infoVideo['items'][0]['contentDetails']['duration']);
                        $video->setTitle($infoVideo['items'][0]['snippet']['title']);
                        $video->setThumbnails($infoVideo['items'][0]['snippet']['thumbnails']);
                        $entityManager->persist($video);
                        $entityManager->flush();
                        $countUpdate++;
                    }
                }
            }
        }

        return new JsonResponse(
            ['rowsUpdate' => $countUpdate],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Actualiza los datos de video ingresados por URL y sin créditos de Youtube
     * @Rest\Patch("/videos/reproductions/{id}")
     * @param Request $request
     * @return JsonResponse
     */
    public function setReproductions(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $video = $entityManager->getRepository(YoutubeVideos::class)->find($id);
        $video->setReproductions($video->getReproductions() + 1);
        $entityManager->persist($video);
        $entityManager->flush();

        return new JsonResponse(
            null,
            JsonResponse::HTTP_NO_CONTENT
        );
    }

    /**
     * @Rest\Get("/videos/ip")
     * @param Request $request
     */
    public function ip(Request $request, ApiYoutube $apiYoutube)
    {
       $data =[];
       if (isset($_SERVER["HTTP_CLIENT_IP"]))
        {
            $data['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CLIENT_IP"];
        }
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
        {
            $data['HTTP_X_FORWARDED_FOR'] = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        if (isset($_SERVER["HTTP_X_FORWARDED"]))
        {
            $data['HTTP_X_FORWARDED'] = $_SERVER["HTTP_X_FORWARDED"];
        }
        if (isset($_SERVER["HTTP_FORWARDED_FOR"]))
        {
            $data['HTTP_FORWARDED_FOR'] = $_SERVER["HTTP_FORWARDED_FOR"];
        }
        if (isset($_SERVER["HTTP_FORWARDED"]))
        {
            $data['HTTP_FORWARDED'] = $_SERVER["HTTP_FORWARDED"];
        }
        if (isset($_SERVER["REMOTE_ADDR"]))
        {
            $data['REMOTE_ADDR'] = $_SERVER["REMOTE_ADDR"];
        }

        return new JsonResponse(
            $data,
            JsonResponse::HTTP_OK
       );
    }


}
