<?php

namespace App\Controller\Web;

use App\Service\ApiYoutube;
use App\Entity\Programation;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(ApiYoutube $apiYoutube)
    {
       // $programacion = $this->getDoctrine()->getRepository(Programation::class)->findNextVideos($apiYoutube);

        return $this->render('home/new.html.twig');
    }

}
