<?php
namespace App\Service;

class ApiYoutube
{
    /**
     * @var \Google_Client()
     */
     private $client = null;

    /**
     * @var string
     */

     private $aplicationName = "api-it-youtube";
    /**
     * @var string
     */

     private $apiKey = "AIzaSyC7CDPlQt2CSK6oeBBDxyucAUsb5Xjl3z0";
    /**
     * @var \Google_Service_YouTube
     */
     private $service = null;
     /**
      * @var int
      */
     private $maxResults = 15;
    /**
     * @var string
     */
    private $baseApi = 'https://www.googleapis.com/youtube/v3/videos';

    /**
     * Método constructor
     * Inicializa el servicio de Youtube Api V3
     */
    public function __construct()
    {
        $this->client = new \Google_Client();
        $this->client->setApplicationName($this->aplicationName);
        $this->client->setDeveloperKey($this->apiKey);
        $this->service = new \Google_Service_YouTube($this->client);
    }

    /**
     * Obtiene lista de video por palabra clave
     */
    public function search(string $q)
    {
        // doc: https://developers.google.com/youtube/v3/docs/search/list
        try {
            return $this->service->search->listSearch('id,snippet', array(
                'q' => $q,
                'regionCode' => 'CO',
                'safeSearch' => 'moderate',
                'maxResults' => $this->maxResults,
            ));
        } catch (\Throwable $th) {
            error_log($th->getMessage());
        }
        
    }
    /**
     * Obtiene información de video por id
     * @param $id string Id de video de youtube
     * @return array
     */
    public function getVideoInfo($id, $part = null)
    {
        $params = array(
            'part' => ($part == null) ? 'snippet,contentDetails' : $part,
            'id' => $id,
            'key' => $this->apiKey,
        );

        $apiUrl = $this->baseApi . '?' . http_build_query($params);
        $result = json_decode(@file_get_contents($apiUrl), true);

        if (!empty($result)) {
            // se convierte la duración del video a segundos
            if (isset($result['items'][0]['contentDetails'])) {
                $interval = new \DateInterval($result['items'][0]['contentDetails']['duration']);
                $result['items'][0]['contentDetails']['duration'] = $interval->h * 3600 + $interval->i * 60 + $interval->s;
            } else {
                $result['items'][0]['contentDetails']['duration'] = null;
            }
        }

        return $result;
    }
}
