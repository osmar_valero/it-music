<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programation
 *
 * @ORM\Table(name="programation", indexes={@ORM\Index(name="youtube_video_id", columns={"youtube_video_id"})})
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProgramationRepository")
 */
class Programation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="dj_ip", type="string", length=100, nullable=false)
     */
    private $djIp;

    /**
     * @var \YoutubeVideos
     *
     * @ORM\ManyToOne(targetEntity="YoutubeVideos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="youtube_video_id", referencedColumnName="id")
     * })
     */
    private $youtubeVideo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDjIp(): ?string
    {
        return $this->djIp;
    }

    public function setDjIp(string $djIp): self
    {
        $this->djIp = $djIp;

        return $this;
    }

    public function getYoutubeVideo(): ?YoutubeVideos
    {
        return $this->youtubeVideo;
    }

    public function setYoutubeVideo(?YoutubeVideos $youtubeVideo): self
    {
        $this->youtubeVideo = $youtubeVideo;

        return $this;
    }


}
