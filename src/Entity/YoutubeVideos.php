<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * YoutubeVideos
 *
 * @ORM\Table(name="youtube_videos")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\YoutubeVideosRepository")
 */
class YoutubeVideos
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_id", type="string", length=100, nullable=false)
     */
    private $youtubeId;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=false, options={"unsigned"=true})
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=400, nullable=false)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(name="reproductions", type="integer", nullable=false, options={"default"="1","unsigned"=true})
     */
    private $reproductions = '1';

    /**
     * @var json
     *
     * @ORM\Column(name="thumbnails", type="json", nullable=false)
     */
    private $thumbnails;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getYoutubeId(): ?string
    {
        return $this->youtubeId;
    }

    public function setYoutubeId(string $youtubeId): self
    {
        $this->youtubeId = $youtubeId;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(int $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getReproductions(): ?int
    {
        return $this->reproductions;
    }

    public function setReproductions(int $reproductions): self
    {
        $this->reproductions = $reproductions;

        return $this;
    }

    public function getThumbnails(): ?array
    {
        return (array) $this->thumbnails;
    }

    public function setThumbnails(array $thumbnails): self
    {
        $this->thumbnails = $thumbnails;

        return $this;
    }
/*
    public function __toString() {
        return (string) $this->getId();
    }
*/

}
