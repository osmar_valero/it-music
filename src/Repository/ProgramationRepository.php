<?php

namespace App\Repository;

use App\Entity\Programation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Programation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Programation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Programation[]    findAll()
 * @method Programation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProgramationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Programation::class);
    }

    /**
     * @return YoutubeVideos[] Returns an array of YoutubeVideos objects
     */
    public function findNextVideos($apiYoutube)
    {
        // Se obtiene la cantidad de videos en cola de reproducción
        $totalProgramation = $this->createQueryBuilder('p')
        ->select('COUNT(p.id)')
        ->getQuery()
        ->getSingleScalarResult();
        
        $limit = 2;
        // Se verifica si hay por lo menos 4 videos en la lista de reproducción
        if ($totalProgramation < 2) {
            $tmpLimit = $limit - $totalProgramation;
            $sql = "SELECT id FROM youtube_videos ORDER BY random() LIMIT $tmpLimit";
            $statement = $this->getEntityManager()->getConnection()->prepare($sql);
            $statement->execute();
            $youtubeVideos = $statement->fetchAll();
            // Se agregan los videos faltantes a la lista de preproducción para completar 4 videos
            foreach ($youtubeVideos as $key => $value) {
                $sql = "INSERT INTO programation (youtube_video_id, dj_ip) VALUES ('".$value['id']."', '0.0.0.0') ";
                $statement = $this->getEntityManager()->getConnection()->prepare($sql);
                $statement->execute();
            }
        }
        // Se retorna los primeros 4 videos de la lista de reproducción
        return $this->createQueryBuilder('p')
            ->innerJoin('p.youtubeVideo', 'vy')
            ->select('p.id, vy.id AS videoId, vy.youtubeId, vy.duration, vy.duration, vy.title, vy.reproductions, vy.thumbnails')
            //->setMaxResults($limit)
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getArrayResult();
    }
    
}
