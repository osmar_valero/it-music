<?php

namespace App\Repository;

use App\Entity\YoutubeVideos;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method YoutubeVideos|null find($id, $lockMode = null, $lockVersion = null)
 * @method YoutubeVideos|null findOneBy(array $criteria, array $orderBy = null)
 * @method YoutubeVideos[]    findAll()
 * @method YoutubeVideos[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class YoutubeVideosRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, YoutubeVideos::class);
    }

    /**
     * Genera infomación por defecto para el video cuando no hay conexión con el Api Youtube
     * @return array
     */
    public function getDefaultInfoVideo()
    {
        return [
            'items' => [
                [
                    'contentDetails' => [
                        'duration' => 234
                    ],
                    'snippet' => [
                        'title' => 'Vídeo en cola de reproducción',
                        'thumbnails' => (array) json_decode('{"default":{"url":"https:\/\/i.ytimg.com\/vi\/T7TJH5MF74s\/default.jpg","width":120,"height":90},"medium":{"url":"https:\/\/i.ytimg.com\/vi\/T7TJH5MF74s\/mqdefault.jpg","width":320,"height":180},"high":{"url":"https:\/\/i.ytimg.com\/vi\/T7TJH5MF74s\/hqdefault.jpg","width":480,"height":360},"standard":{"url":"https:\/\/i.ytimg.com\/vi\/T7TJH5MF74s\/sddefault.jpg","width":640,"height":480},"maxres":{"url":"https:\/\/i.ytimg.com\/vi\/T7TJH5MF74s\/maxresdefault.jpg","width":1280,"height":720}}')
                    ]
                ]
            ]

        ];
    }
    
}
