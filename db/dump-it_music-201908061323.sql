-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: it_music
-- ------------------------------------------------------
-- Server version	5.7.21-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `programation`
--

DROP TABLE IF EXISTS `programation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programation` (
  `youtube_video_id` varchar(100) NOT NULL,
  `dj_ip` varchar(100) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `youtube_video_id` (`youtube_video_id`),
  CONSTRAINT `programation_ibfk_1` FOREIGN KEY (`youtube_video_id`) REFERENCES `youtube_videos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programation`
--

LOCK TABLES `programation` WRITE;
/*!40000 ALTER TABLE `programation` DISABLE KEYS */;
INSERT INTO `programation` VALUES ('0070d9a1-b73c-11e9-8188-409f38e3bb65','127.0.0.1',6),('307b2360-b76e-11e9-8188-409f38e3bb65','127.0.0.1',7);
/*!40000 ALTER TABLE `programation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `youtube_videos`
--

DROP TABLE IF EXISTS `youtube_videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `youtube_videos` (
  `id` varchar(36) NOT NULL,
  `youtube_id` varchar(100) NOT NULL,
  `duration` int(10) unsigned NOT NULL,
  `title` varchar(400) NOT NULL,
  `reproductions` int(10) unsigned NOT NULL DEFAULT '1',
  `thumbnails` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `youtube_videos_youtube_id_IDX` (`youtube_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `youtube_videos`
--

LOCK TABLES `youtube_videos` WRITE;
/*!40000 ALTER TABLE `youtube_videos` DISABLE KEYS */;
INSERT INTO `youtube_videos` VALUES ('0070d9a1-b73c-11e9-8188-409f38e3bb65','0WkAC5ECHA8',205,'Enrique Iglesias & Nicky Jam - El Perdon (\"Iertarea\")',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/0WkAC5ECHA8/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/0WkAC5ECHA8/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/0WkAC5ECHA8/default.jpg\", \"width\": 120, \"height\": 90}, \"standard\": {\"url\": \"https://i.ytimg.com/vi/0WkAC5ECHA8/sddefault.jpg\", \"width\": 640, \"height\": 480}}'),('04334b08-b724-11e9-8188-409f38e3bb65','OvvWwys7nU4',253,'Carlos Vives - Al Filo de Tu Amor (Official Video)',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/OvvWwys7nU4/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"maxres\": {\"url\": \"https://i.ytimg.com/vi/OvvWwys7nU4/maxresdefault.jpg\", \"width\": 1280, \"height\": 720}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/OvvWwys7nU4/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/OvvWwys7nU4/default.jpg\", \"width\": 120, \"height\": 90}, \"standard\": {\"url\": \"https://i.ytimg.com/vi/OvvWwys7nU4/sddefault.jpg\", \"width\": 640, \"height\": 480}}'),('05165f16-4f0d-49f0-be58-9430039badd2','E-iPPQOwMzE',142,'Jessi Uribe - Ok #Repítelas',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/E-iPPQOwMzE/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/E-iPPQOwMzE/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/E-iPPQOwMzE/default.jpg\", \"width\": 120, \"height\": 90}}'),('0f3e6ea6-b733-11e9-8188-409f38e3bb65','OOxAYjR6gv4',325,'Millón a cero l kaleth morales',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/OOxAYjR6gv4/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"maxres\": {\"url\": \"https://i.ytimg.com/vi/OOxAYjR6gv4/maxresdefault.jpg\", \"width\": 1280, \"height\": 720}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/OOxAYjR6gv4/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/OOxAYjR6gv4/default.jpg\", \"width\": 120, \"height\": 90}, \"standard\": {\"url\": \"https://i.ytimg.com/vi/OOxAYjR6gv4/sddefault.jpg\", \"width\": 640, \"height\": 480}}'),('2864b0c4-b615-11e9-be50-409f38e3bb65','RUV55lGPGaY',224,'Yiyo Sarante - Corazòn de Acero Video Oficial',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/RUV55lGPGaY/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/RUV55lGPGaY/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/RUV55lGPGaY/default.jpg\", \"width\": 120, \"height\": 90}}'),('307b2360-b76e-11e9-8188-409f38e3bb65','LbKcHy9cav0',211,'Danny Ocean - Me Rehúso (Official Music Video)',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/LbKcHy9cav0/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"maxres\": {\"url\": \"https://i.ytimg.com/vi/LbKcHy9cav0/maxresdefault.jpg\", \"width\": 1280, \"height\": 720}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/LbKcHy9cav0/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/LbKcHy9cav0/default.jpg\", \"width\": 120, \"height\": 90}, \"standard\": {\"url\": \"https://i.ytimg.com/vi/LbKcHy9cav0/sddefault.jpg\", \"width\": 640, \"height\": 480}}'),('4c6b70c8-b723-11e9-8188-409f38e3bb65','6QSXbvmEEJM',207,'Juanes - La Paga',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/6QSXbvmEEJM/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"maxres\": {\"url\": \"https://i.ytimg.com/vi/6QSXbvmEEJM/maxresdefault.jpg\", \"width\": 1280, \"height\": 720}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/6QSXbvmEEJM/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/6QSXbvmEEJM/default.jpg\", \"width\": 120, \"height\": 90}, \"standard\": {\"url\": \"https://i.ytimg.com/vi/6QSXbvmEEJM/sddefault.jpg\", \"width\": 640, \"height\": 480}}'),('5a6f276d-a8b5-40ea-932c-3b5649419bb6','alqqFtduaFA',253,'Xantos - Bailame Despacio (feat. Dynell)',1,'{\"high\": {\"url\": \"https://i.ytimg.com/vi/QyAQxXucKnw/hqdefault.jpg\", \"width\": 480, \"height\": 360}, \"medium\": {\"url\": \"https://i.ytimg.com/vi/QyAQxXucKnw/mqdefault.jpg\", \"width\": 320, \"height\": 180}, \"default\": {\"url\": \"https://i.ytimg.com/vi/QyAQxXucKnw/default.jpg\", \"width\": 120, \"height\": 90}}');
/*!40000 ALTER TABLE `youtube_videos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'it_music'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-08-06 13:23:47
