# It Music

Proyecto de software para reproducción de video de youtube

Instalación

1. Con la terminal ubicarse en la raíz del proyecto y ejecutar el comando: composer update

2. Crear base de datos en mariadb (nombre sugerido: it_music)

3. Restaurar base de datos, en la raíz del proyecto ejecute: sudo mysql -u root -p it_music <  db/it_music.sql

4. En la raíz del proyecto asignar los datos de conexión a db en el archivo .env

5. Inicie el servidor de Symfony, en la raíz del proyecto ejecute el comando: php bin/console server:run

6. En el navegador digite: http://localhost:8000/  si el paso anterior inicio el servidor podrá ver la página de inicio de la applicación
