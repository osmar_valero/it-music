var base_url = window.location.origin;
var id = null; // id de lista de reproducción
var id_vid = null; // id de video de Youtube
var id_video = null; // id de video en base de datos

$(document).ready(function() {
    // Inicia el reproductor de video
    next_videos();
    // Buscar Canciones
    $("#btn_search").on('click ', function () {
        $('.list-search-youtube').empty();
        $.ajax({
            url: base_url+'/api/videos',
            type: 'GET',
            dataType: 'json',
            data: {q: $('#q').val()}
        })
        .done(function(data) {
            $.each(data, function(index, val) {
                $('.list-search-youtube').append(
                    '<div class="row">'
                        +'<div class="col-6">'
                           +'<img onclick="show_current_video(\''+val.video_id+'\')" src="'+val.thumbnails.medium.url+'" alt="" height="'+val.thumbnails.medium.height+'" width="'+val.thumbnails.medium.width+'">'
                        +'</div>'
                        +'<div class="col-6">'
                            +'<p>'+val.title+'</p>'
                            +'<button onclick="push_video(\''+val.video_id+'\')" data-video_id="'+val.video_id+'" type="button" class="btn btn-info push-video">Programar</button>'
                        +'</div>'
                    +'</div>'
                );
            });
        })
        .fail(function() {
            console.log("error");
            Notiflix.Notify.Warning('Oops!, parece que perdimos comunicación con Youtube. Pero no te preocupes puedes usar la opción de pegar URL.');
        })
        .always(function() {
            console.log("complete");
        });
    });
    // Modal para pegar URL de Youtube
    $('.paste_id_youtube, .playlist_add').on('click', function (event) {
        $('#paste_url').modal();
    })
    $('#btn_program_by_url').on('click', function (event) {
        let url_video_youtube = $('#url_video_youtube').val();
        if (url_video_youtube.indexOf('&list=') == -1) {
            let video_id = url_video_youtube.split("www.youtube.com/watch?v=")[1];
            push_video(video_id);
        } else {
            Notiflix.Notify.Failure('URL incorrecta verfica que no corresponda a una lista de reproducción o no se haya copiado completa');
        }
        $('#url_video_youtube').val('');
    });
    $('.playlist_refresh').on('click', function(){
        refresh_playlist();
    });
    $('.next').on('click', function () {
        // elimina de la programacón el viceo actual
        delete_programation();
        // Se suma 1 reproducción al video actual
        set_reproductions();
        // Se da un tiempo para asegurar la actualización de la lista de reproducción
        setTimeout(next_videos, 1000);
    });

});

/**********************************************  FUNCTIONS  *****************************************/
/**
 * Agrega Videos a la lista de reproducción
 * @param string video_id 
 */
function push_video(video_id) {
    $(this).prop( "disabled", true );
    $.ajax({
        url: base_url+'/api/videos',
        type: 'POST',
        dataType: 'json',
        data: {id: video_id}
    })
    .done(function(data) {
        $(this).prop( "disabled", false );
        if (data.error) {
            Notiflix.Notify.Failure(data.error);
        }
        if (data.success) {
            Notiflix.Notify.Success(data.success);
            refresh_playlist();
        }
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}
/**
 * Obtinen todos los vídeos en cola de reproducció
 */
function next_videos(reload_player = true) {
    // Se actualiza la lista de reproducción
    $.ajax({
        url: base_url+'/api/programation',
        type: 'GET',
        dataType: 'json',
        data: {}
    })
    .done(function(data) {
        // asigna el id de programación
        id = data[0].id;
        // asigana el id video a reproducir db
        id_video = data[0].videoId;
        // asigana el nuevo video a reproducir youtube
        id_vid = data[0].youtubeId;
        // Crear nuevos players
        $("#player").replaceWith('<div id="player"></div>');
        let player = null;
        player = new YT.Player('player', {
            autoplay: 1,
            controls: 0,
            height: '396',
            modestbranding: 0,
            origin: base_url,
            rel: 0, // videos relacionados al finalizar el video
            videoId: id_vid,
            width: '650',
            events: {
                'onReady': onPlayerReady,
                'onStateChange': onPlayerStateChange,
                //'onError': onPlayerError
            }
        });

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', playerOptions);
        }
        function onPlayerReady(event) {
            event.target.playVideo();
        }
        function onPlayerStateChange(event) {
            if(event.data === 0) {
                player.destroy();
                // Se elimina de la lista el video que ya se reprodujo
                delete_programation();
                // Se suma 1 reproducción al video actual
                set_reproductions();
                // Se da un tiempo para asegurar la actualización de la lista de reproducción
                setTimeout(next_videos, 1000);
            }
        }
        function onPlayerError (event) {
            let error_video = '';
        
            if (event.data == 2) {
                error_video = 'Youtube nos informa que el identificador del Vídeo no es valido.';
            }
            else if (event.data == 100) {
                error_video = 'Youtube nos informa que no fue posible encontrar el vídeo.';
            } else { // para cod error 101 y 150
                error_video ='Youtube nos informa que no tenemos permiso para reproducir el vídeo en este sitio.';
            }

            player.destroy();
            // Se elimina de la lista el video que ya se reprodujo
            delete_programation();
            delete_video();
            //next_videos();
            setTimeout(next_videos, 1000);
            Notiflix.Notify.Init({timeout: 10000});
            Notiflix.Notify.Failure(error_video);
        }

        refresh_playlist(data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}
/**
 * Actualiza la sección de lista de reproducción
 * @param json data 
 */
function refresh_playlist(data = []) {
    $('#play_list').empty();
    if (data.length > 0 ) {
        $.each(data, function( index, value ) {
            index++;
            if (index > 1) {
                let min = (value.duration / 60).toFixed(1);
                $('#play_list').append(
                    '<div class="row next-video">'
                        +'<div class="col-md-3">'
                            +'<img class="img-next-video" src="'+value.thumbnails.medium.url+'" alt="'+value.title+'" >'
                        +'</div>'
                        +'<div class="col-md-9 text-left">'
                            +'<p>'+value.title+'</p>'
                            +'<time class="thumbnail-classic-time" datetime="2019-09-19">'+min+' Min</time>'
                        +'</div>'
                    +'</div>'
                );
            }
        });
    } else {
        $.ajax({
            url: base_url+'/api/programation',
            type: 'GET',
            dataType: 'json',
            data: {}
        })
        .done(function(data) {
            $.each(data, function( index, value ) {
                index++;
                if (index > 1) {
                    let min = (value.duration / 60).toFixed(1);
                    $('#play_list').append(
                        '<div class="row next-video">'
                            +'<div class="col-md-3">'
                                +'<img class="img-next-video" src="'+value.thumbnails.medium.url+'" alt="'+value.title+'" >'
                            +'</div>'
                            +'<div class="col-md-9 text-left">'
                                +'<p>'+value.title+'</p>'
                                +'<time class="thumbnail-classic-time" datetime="2019-09-19">'+min+' Min</time>'
                            +'</div>'
                        +'</div>'
                    );
                }
            });
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
    }    
}
/**
 * Muestra la ventana para reproducir el video seleccinado de la lista de búsqueda
 * @param string video_id 
 */
function show_current_video(video_id) {
    $('#current_preview_video').attr('src', 'https://www.youtube.com/embed/'+video_id);
    $('#preview_video').modal();
}
/**
 * Elimina el vídeo reproducido de la lista de reproducción
 */
function delete_programation() {
    $.ajax({
        url: base_url+'/api/programation/'+id,
        type: 'DELETE',
        dataType: 'json',
        data: {}
    })
    .done(function(data) {
        console.log('Delete programation: '+id);
    })
    .fail(function() {
        console.log("error al eliminar último vídeo");
    })
    .always(function() {
        console.log("complete");
    });
}
/**
 * Actualiza la cantidad de reproducciones del vídeo reproducido
 */
function set_reproductions (){
    $.ajax({
        url: base_url+'/api/videos/reproductions/'+id_video,
        type: 'PATCH',
        dataType: 'json',
        data: {}
    })
    .done(function(data) {
        // Done
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}
/**
 * Elimina un video si no se puede reproducir
 */
function delete_video () {
    $.ajax({
        url: base_url+'/api/videos/'+id_video,
        type: 'DELETE',
        dataType: 'json',
        data: {}
    })
    .done(function(data) {
        console.log('Delete  bad vídeo: '+id_video);
    })
    .fail(function() {
        console.log("error al eliminar video");
    })
    .always(function() {
        console.log("complete");
    });
}

